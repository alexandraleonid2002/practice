﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Colculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();          
        }
        static void Menu()
        {
            List<string> Menuitems = new List<string>()
            {
                "1.Умножение",
                "2.Сложение" ,
                "3.Вычетание",
                "4.Перевод целых чисел в другую систему счисления" ,
                "5.Перевод целых чисел в римскую систему счисления",
                "6.Перевод из римской системы счислений"
            };
            Console.CursorVisible = false;
            do
            {
                string selectedMenu = DrawMenu(Menuitems);
                switch (selectedMenu)
                {
                    case "1.Умножение":
                        Console.Clear();
                        Multiplication();
                        Console.Read();
                        break;
                    case "2.Сложение":
                        Console.Clear();
                        Addition();
                        Console.Read();
                        break;
                    case "3.Вычетание":
                        Console.Clear();
                        Subtraction();
                        Console.Read();
                        break;
                    case "4.Перевод целых чисел в другую систему счисления":
                        Console.Clear();
                        Convert();
                        Console.Read();
                        break;
                    case "5.Перевод целых чисел в римскую систему счисления":
                        Console.Clear();
                        ToRome();
                        Console.Read();
                        break;
                    case "6.Перевод из римской системы счислений":
                        Console.Clear();
                        FromRome();
                        Console.Read();
                        break;
                }
            } while (true);
        }
        static int Index = 0;
        static string DrawMenu(List<string> items)
        {
            Console.Clear();
            for (int i = 0; i < items.Count; i++)
            {
                if (i == Index)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(items[i]);
                }
                else
                {
                    Console.WriteLine(items[i]);
                }
                Console.ResetColor();
            }

            ConsoleKeyInfo ckey = Console.ReadKey();
            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (Index == items.Count - 1)
                {
                    Index = 0;
                }
                else
                {
                    Index++;
                }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (Index == 0)
                {
                    Index = items.Count - 1;
                }
                else
                {
                    Index--;
                }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return items[Index];
            }
            else
            {
                return "";
            }
            Console.Clear();
            return "";
        }
        static void CheckSS(int ss)
        {
            if (ss > 50 || ss < 2)
            {
                throw new Exception("Выход за предел допустимых значений");
            }
        }
        static void Convert()
        {
            Console.CursorVisible = true;
            Console.Write("Введите целое число ");
            int number = int.Parse(Console.ReadLine());
            Console.Write("Введите из какой системы счисления переводить: ");
            int startSystem = int.Parse(Console.ReadLine());
            CheckSS(startSystem);
            Console.Write("Введите в какую систему счисления переводить: ");
            Console.CursorVisible = false;
            int finishSystem = int.Parse(Console.ReadLine());
            CheckSS(finishSystem);
            if (startSystem == 10)
            {
                
                Console.Clear();
                Console.WriteLine("");
                string result = ConvertFrom10(number, finishSystem);
                Console.WriteLine("Ответ: " + result);
            }
            else if (finishSystem == 10) 
            {
                Console.Clear();
                Console.WriteLine("");
                double result = ConvertTo10(number, startSystem);
                Console.WriteLine("Ответ: " + result);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("");
                int midNumber = (int)ConvertTo10(number, startSystem);
                Console.WriteLine("\n");
                string result = ConvertFrom10(midNumber, finishSystem);
                Console.WriteLine("\nОтвет:" + result);
            }
        }
        static string ConvertFrom10(int number, int finishSistem)
        {
            Console.WriteLine("Целая часть числа находится делением на основание новой");
            StringBuilder x = new StringBuilder();
            while (number > 0)
            {
                int n = number % finishSistem;
                Console.WriteLine($"Делим {number} на {finishSistem}, получаем остаток {n}");              
                x.Insert(0, n);
                number /= finishSistem;
            }
            Console.WriteLine($"Записываем остатки в обратном порядке, получаем {x}");
            return x.ToString();
        }
        static double ConvertTo10(int number, int startSistem)
        {
            Console.WriteLine("Переводим число в 10-ную систему счисления");
            Console.WriteLine("Записываем число развернутой форме, т.е. в виде суммы степеней системы счисления(В обратном порядке с 0) с коэффициентами-цифраи, и находим эту сумму");
            Console.WriteLine();
            StringBuilder sum = new StringBuilder();
            double n = 0;
            int i = 0;
            while (number > 0)
            {
                int bit = number % 10;
                sum.Append($"{bit} * {startSistem}^{i} + ");
                n += bit * Math.Pow(startSistem, i);
                number /= 10;
                i += 1;
            }
            sum.Remove(sum.Length -2, 2);
            Console.WriteLine(sum + " = " + n );
           
            return n;
        }
        static void FromRome()
        { 
            Console.Clear();
            Console.CursorVisible = true;
            Console.Write("Введите число в римской системе счисления: ");
            string start = Console.ReadLine();
            Console.CursorVisible = false;
            List<string> number = new List<string>();
            foreach (char b in start)
            {
                number.Add(b.ToString());
            }
            int result = 0;
            Console.WriteLine("Переводим все буквы и суммируем");
            StringBuilder sum = new StringBuilder();
            for (int i = 0; i < number.Count-1; i++ )
            {
                int nl = RomeIs(number[i]);
                int nR = RomeIs(number[i+1]);
                if (nl < nR)
                {
                    Console.WriteLine($"{nl} меньше {nR}, значит вычитаем {nl} из {nR} и прибавляем {nR-nl}");
                    result  += nR - nl;
                    sum.Append((nR - nl) + " + ");
                    Console.WriteLine(result);
                }
                else
                {
                    Console.WriteLine($"{nl} больше {nR}, прибавляем {nl}");
                    result += nl;
                    sum.Append(nl + " + ");
                    Console.WriteLine(result);
                }               
            }
           
            sum.Remove(sum.Length - 2, 2);
            Console.WriteLine(sum + " = " + result);
            Console.WriteLine("Ответ: " + result);
        }
        static int RomeIs(string bit)
        {
            int n = 0;
            if ("IVXLCDM".Contains(bit))
            {
                switch (bit)
                {
                    case "I":
                        n = 1;
                        break;
                    case "V":
                        n = 5;
                        break;
                    case "X":
                        n = 10;
                        break;
                    case "L":
                        n = 50;
                        break;
                    case "C":
                        n = 100;
                        break;
                    case "D":
                        n = 500;
                        break;
                    case "M":
                        n = 1000;
                        break;
                }
            }
            else
            {
                throw new Exception("WRONG FORMART");
            }
            return n;
        }
        static void ToRome()
        {
            Console.Clear();
            Console.CursorVisible = true;
            Console.Write("Введите число: ");
            int number = int.Parse(Console.ReadLine());
            if (number > 5000 || number < 1)
            {
                throw new Exception("Выход за предел допустимых значений");
            }
            Console.CursorVisible = false;
            string[] letters = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "VI", "V", "IV", "I" };
            int[] values = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 6, 5, 4, 1 };

            StringBuilder result = new StringBuilder();
            Console.WriteLine("Вычетаем из числа максимальные значения римской системы счисления (которые не больше самого числа), пока оно не станет  равным нулю ");
            for ( int i = 0; i < values.Length; i++)
            {
                while(number >= values[i])
                {
                    Console.WriteLine("Число " + number);
                    Console.WriteLine($"Вычетаем и записываем {values[i]}, то есть {letters[i]}");
                    number -= values[i];
                    result.Append(letters[i]);
                }
            }
            Console.WriteLine("Ответ: " + result);
        }
        static void Addition()
        {
            Console.CursorVisible = true;
            (string number1, string number2, int ss) = GetInfoToAdd();
            Console.CursorVisible = false;
            StringBuilder result = new StringBuilder();
            Console.WriteLine("Складываем числа побитово");
            Dictionary<string, int> transfer = Transfering();
            int b1;
            int b2;
            string bit;
            if (number1.Length > number2.Length)
            {
                for (int i = 0; i < number1.Length - number2.Length; i++)
                {
                    number2 = "0" + number2;
                }
            }
            else
            {
                for (int i = 0; i < number2.Length - number1.Length; i++)
                {
                    number1 = "0" + number1;
                }
            }
            int p = 0;
            while(number1.Length > 0)
            {
                b1 = LetterToNumber(number1, transfer);
                b2 = LetterToNumber(number2, transfer);

                Console.WriteLine($"Складываем биты {b1} и {b2}");
                if (b1 + b2 + p >= ss)
                {
                    Console.WriteLine($"{b1 + b2 + p} не меньше {ss}, поэтому записываем {b1 + b2 + p - ss}  и переносим 1");
                    bit = NumberToLetter(b1 + b2 + p - ss, transfer);
                    p = 1;
                }
                else
                {
                    Console.WriteLine($"Записываем {b1 + b2 + p} ");
                    bit = NumberToLetter(b1 + b2 + p, transfer);
                    Console.WriteLine(bit);
                    p = 0;
                }
                result.Insert(0, bit);
                number1 = number1.Remove(number1.Length - 1);
                number2 = number2.Remove(number2.Length - 1);
            }
            if(p!= 0)
            {
                result.Insert(0, p.ToString());
            }
            Console.WriteLine("Ответ: " + result.ToString());
        }
        static string NumberToLetter(int number, Dictionary<string, int> transfer)
        {
            if (number >= 10)
            {
                return ToLetter(number, transfer);
            }
            else
            {
                return number.ToString();
            }
        }
        static int LetterToNumber(string number, Dictionary<string,int> transfer)
        {
            int b;
            if (int.TryParse(number [^ 1].ToString(), out int num))
            {
                b = num;
            }
            else
            {
                Console.WriteLine("Переводим букву в число");
                b = ToNumber(number[^1].ToString(), transfer);
            }
            return b;
        }
        static (string, string, int) GetInfoToAdd()
        {
            Console.Write("Введите первое число: ");
            string number1 = Console.ReadLine();
            Console.Write("Введите второе число: ");
            string number2 = Console.ReadLine();
            Console.Write("Систему счисления: ");
            int ss = int.Parse(Console.ReadLine());
            CheckSS(ss);
            return (number1, number2, ss);
        }
        static int ToNumber(string letter, Dictionary<string, int> transfer)
        {            
            return transfer[letter];
        }
        static string ToLetter(int number, Dictionary<string, int> transfer)
        { 
            string[] letters = { "A", "B", "C", "D", "E", "F","G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R","S", "T", 
                                 "U", "V", "W", "X", "Y", "Z", "Б", "Г", "Д", "Ж", "З", "И", "Л", "П", "У", "Ц", "ч", "Ш", "Щ", "Э", "Ю"};
            int[] numbers = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
                                        37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50 };

            for (int i = 0; i < letters.Length; i++)
            {
                if (number == numbers[i])
                {
                    return (string)letters[i];
                }
                
            }
            throw new Exception("НУ ЧТО НЕ ТАК?!");
        }
        static Dictionary<string, int> Transfering()
        {
            Dictionary<string, int> transfer = new Dictionary<string, int>
            {
                {"A", 10},
                {"B", 11},
                {"C", 12},
                {"D", 13},
                {"E", 14},
                {"F", 15},
                {"G", 16},
                {"H", 17},
                {"I", 18},
                {"J", 19},
                {"K", 20},
                {"L", 21},
                {"M", 22},
                {"N", 23},
                {"O", 24},
                {"P", 25},
                {"Q", 26},
                {"R", 27},
                {"S", 28},
                {"T", 29},
                {"U", 30},
                {"V", 31},
                {"W", 32},
                {"X", 33},
                {"Y", 34},
                {"Z", 35},
                {"Б", 36},
                {"Г", 37},
                {"Д", 38},
                {"Ж", 39},
                {"З", 40},
                {"И", 41},
                {"Л", 42},
                {"П", 43},
                {"У", 44},
                {"Ц", 45},
                {"ч", 46},
                {"Ш", 47},
                {"Щ", 48},
                {"Э", 49},
                {"Ю", 50},

            };
            return transfer;
        }
        static void Subtraction()
        {
            Console.CursorVisible = true;
            (string number1, string number2, int ss) = GetInfoToAdd();
            Console.CursorVisible = false;
            StringBuilder result = new StringBuilder();
            Console.WriteLine("Вычитаем числа побитово");
            Dictionary<string, int> transfer = Transfering();
            int b1;
            int b2;
            string bit;
            string sign;
            if (number1.Length >= number2.Length)
            {
                sign = "+";
            }
            else
            {
                string s = number1;
                number1 = number2;
                number2 = s;
                sign = "-";
            }
            int p = 0;
            for (int i = 0; i < number1.Length - number2.Length; i++)
            {
                number2 = "0" + number2;
            }
            while (number1.Length > 0)
            {
                b1 = LetterToNumber(number1, transfer);
                b2 = LetterToNumber(number2, transfer);

                Console.WriteLine($"Вычетаем {b2} из {b1}");
                if (b1 + p >= b2)
                {
                    if (p != -1)
                    {
                        Console.WriteLine($"Записываем {b1 - b2}");
                        bit = NumberToLetter(b1 - b2, transfer);
                        p = 0;
                    }
                    else
                    {
                        Console.WriteLine($"Вычетаем занятый бит. Записываем {b1 + p - b2}");
                        bit = NumberToLetter(b1 + p - b2, transfer);
                        p = 0;

                    }
                }
                else
                {
                    if (p != -1)
                    {
                        Console.WriteLine($"{b1} меньше {b2}. Следовательнно нужно занять {ss} у следующего бита. Вычетаем {b2} из {b1 + ss}");
                        bit = NumberToLetter(b1 + ss - b2, transfer);
                        p = -1;
                    }
                    else
                    {
                        Console.WriteLine($"Вычетаем занятый бит. {b1 + p} меньше {b2}. Следовательнно нужно занять {ss} у следующего бита.");
                        Console.WriteLine($"Вычетаем {b2} из {b1 + p + ss}.Записываем {b1 + p + ss - b2}");
                        bit = NumberToLetter(b1 + p + ss - b2, transfer);
                        p = -1;
                    }
                }
                result.Insert(0, bit);
                number1 = number1.Remove(number1.Length - 1);
                number2 = number2.Remove(number2.Length - 1);
            }
            if (sign == "-")
            {
                result.Insert(0, "-");
                Console.WriteLine("Ответ: " + result.ToString());
            }
            else
            {
                Console.WriteLine("Ответ: " + result.ToString());
            }
        }    
        static void Multiplication()
        {
            Console.CursorVisible = true;
            (string number1, string number2, int ss) = GetInfoToAdd();
            Console.CursorVisible = false;
            Console.WriteLine("Умножаем первое число на каждый бит второго, затем складываем");
            Dictionary<string, int> transfer = Transfering();
            List<string> bits = new List<string>();
            if (number1.Length < number2.Length)
            {
                string s = number1;
                number1 = number2;
                number2 = s;                
            }
            int i = 0;
            while(number2.Length > 0)
            {
                StringBuilder bit = MultiplyOnBit(number1, number2, transfer, ss);
                Console.WriteLine($"Получаем {bit}");
                if (i != 0)
                {
                    for (int j = 0; j < i; j ++)
                    {
                        bit.Append("0");
                    }
                }
                bits.Add(bit.ToString());
                number2 = number2.Remove(number2.Length - 1);

            }
            Console.WriteLine("Складываем все числа");
            string result = AdditBits(bits, ss);
            Console.WriteLine("Ответ: " + result);
        }
        static StringBuilder MultiplyOnBit(string number1,string number2, Dictionary<string, int> transfer, int ss)
        {
            StringBuilder bit = new StringBuilder();
            int p = 0;
            int p1 = 0;
            int b2 = LetterToNumber(number2, transfer);
            Console.WriteLine($"Умножаем {number1} на {b2}");
            while (number1.Length > 0)
            {
                int b1 = LetterToNumber(number1, transfer);
                Console.WriteLine($"Умножаем {b1} на {b2}");
                int umn = b1 * b2 + p;
                if (b1 * b2 + p >= ss)
                {
                    while (umn >= ss)
                    {
                        p1 += 1;
                        umn -= ss;
                    }
                    p = p1;
                    Console.WriteLine($"Записываем {umn}, переносим {p}  на следующий бит");
                    p1 = 0; 
                }
                else
                {
                    p = 0;
                }
                
                bit.Insert(0, NumberToLetter(umn, transfer));
                
                number1 = number1.Remove(number1.Length - 1);
            }
            if (p != 0)
            {
                bit.Insert(0, p);
            }
            p = 0; 
            return bit;
        }
        static string AdditBits(List<string> bits, int ss)
        {
            int i = 0;
            string number = bits[0];
            while(i<bits.Count - 1)
            {               
                StringBuilder result = new StringBuilder();               
                Dictionary<string, int> transfer = Transfering();
                int b1;
                int b2;
                string bit;
                Console.WriteLine($"Скалдываем числа {number} и {bits[i+1]}");
                for(int j= 0; j<= i; j++)
                {
                    bits[i + 1] = bits[i + 1] + "0";
                }
                if (number.Length > bits[i + 1].Length)
                {
                    for (int j = 0; j < number.Length - bits[i + 1].Length; j++)
                    {
                        bits[i + 1] = "0" + bits[i + 1];
                    }
                }
                else
                {
                    for (int j = 0; j < bits[i + 1].Length - number.Length; j++)
                    {
                        number = "0" + number;
                    }
                }
                Console.WriteLine(bits[i + 1] + "+" + number);
                int p = 0;
                result.Remove(0, result.Length);
                while (number.Length > 0)
                {
                    b1 = LetterToNumber(number, transfer);
                    b2 = LetterToNumber(bits[i + 1], transfer);

                    Console.WriteLine($"Складываем биты {b1} и {b2}");
                    if (b1 + b2 + p >= ss)
                    {
                        Console.WriteLine($"{b1 + b2 + p} не меньше {ss}, поэтому записываем {b1 + b2 + p - ss}  и переносим 1");
                        bit = NumberToLetter(b1 + b2 + p - ss, transfer);
                        p = 1;
                    }
                    else
                    {
                        Console.WriteLine($"Записываем {b1 + b2 + p} ");
                        bit = NumberToLetter(b1 + b2 + p, transfer);
                        p = 0;
                    }
                    result.Insert(0, bit);
                    number = number.Remove(number.Length - 1);
                    bits[i + 1] = bits[i + 1].Remove(bits[i + 1].Length - 1);
                }
                if (p != 0)
                {
                    result.Insert(0, p.ToString());
                }
                number = result.ToString();
                i += 1;
            }
            return number;            
        }
    }
}
